from collections import Counter
import pandas as pd
from pandas._testing import assert_frame_equal

# importing CSV files
try:
    df_data = pd.read_csv('src/data.csv')
except FileNotFoundError:
    print("File data not found")

try:
    df_curr = pd.read_csv('src/currencies.csv')
except FileNotFoundError:
    print("File currencies not found")

try:
    df_match = pd.read_csv('src/matching.csv')
except FileNotFoundError:
    print("File matching not found")

# merging and saving CSV files: data, currency & matching
df_out = df_data.merge(df_curr, on='currency')
df_out = df_out.merge(df_match, on='matching_id')

# adding and initialize new columns to dfout
df_out['total_price_PLN'] = df_out['price'] * df_out['quantity'] * df_out['ratio']
df_out['currency'] = 'PLN'

# matching_id occurrences
counter_dict = Counter(df_out['matching_id'])
counter_df = pd.DataFrame(index=list(counter_dict.keys()), data=list(counter_dict.values()))
df_match.set_index('matching_id', inplace=True)
joined_df = df_match.join(counter_df, lsuffix='_caller', rsuffix='_counter_df')
joined_df.columns = ['top_priced_count', 'matching_ids_count']

# number of ignored products
joined_df['ignored_products_count'] = joined_df['matching_ids_count'] - joined_df['top_priced_count']

# finding smallest product
matching_ids_list = list(set(df_out['matching_id']))
ignored_products_list = list(joined_df['ignored_products_count'])
final_results = df_out[df_out['matching_id'] ==
                    matching_ids_list[0]].sort_values(by=['total_price_PLN'])[ignored_products_list[0]:]

# cutting smallest ignored products
for num in range(1, len(matching_ids_list)):
    final_results = final_results.append(df_out[df_out['matching_id'] ==
                        matching_ids_list[num]].sort_values(by=['total_price_PLN'])[ignored_products_list[num]:])

# sum price
sum_df = final_results.groupby('matching_id')['total_price_PLN'].sum().reset_index().\
            rename(columns=({'total_price_PLN': 'total_price'}))

# average price
avg_df = final_results.groupby('matching_id')['total_price_PLN'].mean().reset_index().\
            rename(columns=({'total_price_PLN': 'avg_price'}))

# merging to joined_df
joined_df = joined_df.merge(sum_df, on='matching_id')
joined_df = joined_df.merge(avg_df, on='matching_id')

# adding currency to results
joined_df['currency'] = "PLN"

# dropping useless columns
joined_df.drop(columns=["top_priced_count", "matching_ids_count"], inplace=True)

# reordering table
final_table = joined_df[['matching_id', 'total_price', 'avg_price', 'currency', 'ignored_products_count']]

# saving final result to CSV
final_table.to_csv('src/top_products.csv', index=False)


# expected values
excpected = pd.DataFrame({'matching_id': [1, 2, 3],
                          'total_price': [12285.0, 28350.0, 27720.0],
                          'avg_price': [6142.5, 14175.0, 9240.0],
                          'currency': ['PLN', 'PLN', 'PLN'],
                          'ignored_products_count': [1, 0, 1]})

# testing if final table is equal to expected
def test_final_table():
    pd.testing.assert_frame_equal(final_table, excpected)

if __name__ == '__main__':
    test_final_table()